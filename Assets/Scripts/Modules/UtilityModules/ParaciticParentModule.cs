using UnityEngine;

/// <summary>
/// A module that follows its first child around, so that that child does not change
/// local position.
/// </summary>
public class ParaciticParentModule : Module
{
    public GameObject firstChild;

    private void Start()
    {
        firstChild = transform.GetChild(0).gameObject;
    }

    private void FixedUpdate()
    {
        transform.position = firstChild.transform.position;
        firstChild.transform.localPosition = Vector3.zero;
    }
}