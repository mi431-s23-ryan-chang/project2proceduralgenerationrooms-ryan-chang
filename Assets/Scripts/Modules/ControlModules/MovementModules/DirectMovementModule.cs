﻿using System.Collections;
using UnityEngine;

public class DirectMovementModule : MovementModule
{
    [Header("User settings")]
    [Tooltip("Acceleration of movement.")]
    public float acceleration = 5;

    public override void MovementPressed(Vector3 movement)
    {
        entity.c3d.material = null;
    }

    public override void MovementStop(Vector3 movement)
    {
        print("stop");
        entity.c3d.material = Resources.Load<PhysicMaterial>("PhysicMaterials/EntityStop");
    }

    public void FixedUpdate()
    {
        if (!(CurrentMovement.x.Approx(0) && CurrentMovement.z.Approx(0)))
        {
            entity.r3d.AddRelativeForce(CurrentMovement * acceleration);
            entity.r3d.velocity = Vector3.ClampMagnitude(entity.r3d.velocity, maxSpeed);
        }
    }
}