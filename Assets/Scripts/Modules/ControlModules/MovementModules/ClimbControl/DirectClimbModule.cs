using UnityEngine;

public class DirectClimbModule : ClimbControlModule
{
    [Header("User settings")]
    [Tooltip("The force applied to an entity that is climbing.")]
    public float climbForce = 15;

    private void FixedUpdate()
    {
        if (canClimb)
        {
            if (CurrentMovement.y > 0)
            {
                entity.r3d.AddForce(0, CurrentMovement.y * climbForce, 0, ForceMode.Acceleration);
            }
        }
    }
}