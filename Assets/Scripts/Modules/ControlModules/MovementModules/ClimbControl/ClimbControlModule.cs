using UnityEngine;

public abstract class ClimbControlModule : SecondaryControlModule
{
    [Tooltip("Set to true on contact with a ladder or other climbable object.")]
    public bool canClimb;
}