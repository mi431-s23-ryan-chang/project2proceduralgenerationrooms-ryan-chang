﻿using System.Collections;
using UnityEngine;

public abstract class JumpControlModule : SecondaryControlModule
{
    [Header("User settings")]
    [Tooltip("Force of jump")]
    public float jumpForce = 15;

    [Tooltip("Mutliplied by jumpForce to obtain the true jump direction.")]
    public Vector3 jumpMultiplier = Vector3.up;

    [Tooltip("After how many seconds after jumping can traction be regained with the ground?")]
    public float jumpGraceTime = 1f;
}