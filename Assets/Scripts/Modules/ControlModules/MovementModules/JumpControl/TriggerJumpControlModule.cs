using UnityEngine;

public class TriggerJumpControlModule : JumpControlModule
{
    [Tooltip("Colliders to check for things to jump off of.")]
    public GroundCheck[] groundChecks;

    public override void JumpPressed()
    {
        Vector3 jumpMovement = new Vector3(CurrentMovement.x, 1, CurrentMovement.z);

        foreach (var check in groundChecks)
        {
            if (check.IsGrounded)
            {
                var jumpDir = new Vector3(
                    jumpMovement.x * jumpMultiplier.x,
                    jumpMovement.y * jumpMultiplier.y,
                    jumpMovement.z * jumpMultiplier.z);
                entity.r3d.AddForce(jumpDir * jumpForce, ForceMode.Impulse);
                break;
            }
        }
    }
}