using UnityEngine;
using UnityEngine.InputSystem;

public class PointAtMovementModule : Module
{
    public enum PointAtWhat
    {
        Transform,
        Cursor
    }

    [Header("User settings")]
    [Tooltip("What to point at.")]
    public PointAtWhat pointAtWhat;

    [Tooltip("How fast to look at a thing.")]
    public float lerpSpeed = float.PositiveInfinity;

    private void FixedUpdate()
    {
        switch (pointAtWhat)
        {
            case PointAtWhat.Cursor:
                var viewpointPos = Camera.main.ScreenToViewportPoint(Mouse.current.position.ReadValue());
                
                var centered = viewpointPos - new Vector3(0.5f, 0.5f, 0.5f);

                // Camera
                Camera.main.transform.Rotate(new Vector3(-centered.y, 0));
                Vector3 rotation = new Vector3(-centered.y, centered.x, 0);
                transform.Rotate(rotation, Space.Self);

                var currRot = Camera.main.transform.rotation.eulerAngles;
                var xRot = currRot.x.ClampAngle(-60, 60);
                Camera.main.transform.rotation = Quaternion.Euler(xRot, currRot.y, currRot.z);

                // Entity
                entity.transform.Rotate(new Vector3(0, centered.x));
                currRot = entity.transform.rotation.eulerAngles;

                if (entity.r3d.constraints.HasFlag(RigidbodyConstraints.FreezeRotationX))
                    currRot.x = 0;

                if (entity.r3d.constraints.HasFlag(RigidbodyConstraints.FreezeRotationY))
                    currRot.y = 0;

                if (entity.r3d.constraints.HasFlag(RigidbodyConstraints.FreezeRotationZ))
                    currRot.z = 0;
                
                entity.transform.rotation = Quaternion.Euler(currRot.x, currRot.y, currRot.z);

                break;
        }
    }
}