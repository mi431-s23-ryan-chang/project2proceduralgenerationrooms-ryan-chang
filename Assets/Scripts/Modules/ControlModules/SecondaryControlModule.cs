﻿using UnityEngine;

/// <summary>
/// Accepts input from master control modules, which will then be used to
/// control stuff
/// </summary>
public abstract class SecondaryControlModule : Module
{
    public Vector3 CurrentMovement { get; set; }

    /// <summary>
    /// Called when movement is pressed
    /// </summary>
    /// <param name="movement">The movement vector</param>
    public virtual void MovementPressed(Vector3 movement) { }

    /// <summary>
    /// Called when movement stops
    /// </summary>
    /// <param name="movement">The movement vector at the time of stoppage</param>
    public virtual void MovementStop(Vector3 movement) { }

    /// <summary>
    /// Called when the left mouse button is clicked
    /// </summary>
    public virtual void Fire1Pressed() { }

    /// <summary>
    /// Called when the left mouse button stops being clicked
    /// </summary>
    public virtual void Fire1Stop() { }

    /// <summary>
    /// Called when the right mouse button is clicked
    /// </summary>
    public virtual void Fire2Pressed() { }

    /// <summary>
    /// Called when the right mouse button stops being clicked
    /// </summary>
    public virtual void Fire2Stop() { }

    /// <summary>
    /// Called when the middle mouse button is clicked
    /// </summary>
    public virtual void Fire3Pressed() { }

    /// <summary>
    /// Called when the middle mouse button stops being clicked
    /// </summary>
    public virtual void Fire3Stop() { }

    /// <summary>
    /// Called when the space bar is pressed.
    /// </summary>
    public virtual void JumpPressed() { }

    /// <summary>
    /// Called when the space bar is released.
    /// </summary>
    public virtual void JumpStop() { }

    // public virtual void OnDie() {}
}
