using UnityEngine;

public class AnimControlModule : SecondaryControlModule
{
    [Header("User settings")]
    [Tooltip("The animation. Requires Speed (float) variable and Direction (float).")]
    public Animator anim;

    private void FixedUpdate()
    {
        Vector2 curMove = new Vector2(CurrentMovement.x, CurrentMovement.z);
        Vector2 curDir = new Vector2(transform.forward.x, transform.forward.z);
        Vector2 curRight = new Vector2(transform.right.x, transform.right.z);
        Vector2 curV = new Vector2(entity.r3d.velocity.x, entity.r3d.velocity.z);

        // Debug.DrawRay(transform.position, curDir, Color.black);

        if (curV.magnitude > 0.1)
        {
            // sAngle is the angle between curDir and curV.
            float sAngle = Vector2.SignedAngle(curDir, curV);
            float sAnglePM = sAngle.AsPlusMinus180();
            // print($"[-180, 180]: {sAnglePM}, [0, 360]: {sAngle.AsPositiveDegrees()}");

            float leanAmount = Vector2.Dot(curRight, curV);

            // print($"Lean amount: {leanAmount}");

            float vMag = curV.magnitude;

            anim.SetFloat("Speed", vMag);
            anim.SetFloat("Velocity", curMove.y < 0 ? -1 : 1);
            anim.SetFloat("Direction", leanAmount);
        }
        else
        {
            anim.SetFloat("Speed", 0);
            anim.SetFloat("Velocity", 1);
            anim.SetFloat("Direction", 0);
        }
    }
}