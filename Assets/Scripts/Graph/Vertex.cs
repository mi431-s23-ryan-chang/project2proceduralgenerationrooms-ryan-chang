using System.Collections.Generic;

/// <summary>
/// Vertex used in graph. Weights are stored in the graph.
/// </summary>
/// <typeparam name="T">Any type. For use in graph, ensure it is IEquatable.</typeparam>
public class Vertex<T>
{
    /// <summary>
    /// This is unique for each vertex
    /// and serves as the id. 
    /// </summary>
    public T id;

    /// <summary>
    /// Dictionary of outgoing edges. <id, graph weight>.
    /// </summary>
    public Dictionary<T, float> adjacent = new Dictionary<T, float>();

    /// <summary>
    /// Used in the search algorithms. Key is the ID of the thing using the
    /// vertex, bool is if it has been visited or not.
    /// </summary>
    private Dictionary<string, bool> visited = new Dictionary<string, bool>();

    /// <summary>
    /// Dictionary of tags this vertex has.
    /// </summary>
    public HashSet<string> tags = new HashSet<string>();

    /// <summary>
    /// Degree of this vertex (number of outgoing edges).
    /// </summary>
    public int Degree => adjacent.Count;

    /// <summary>
    /// Default heuristic (none) for a vertex.
    /// </summary>
    /// <returns>0</returns>
    private int NoHeuristic() { return 0; }

    public delegate int Heuristic();

    /// <summary>
    /// Heuristic for A* search.
    /// </summary>
    public Heuristic heuristic;

    /* Constructors */
    /// <summary>
    /// Creates a vertex.
    /// </summary>
    /// <param name="id">ID of this vertex.</param>
    public Vertex(T id)
    {
        this.id = id;
        heuristic = NoHeuristic;
    }

    /// <summary>
    /// Creates a vertex with a custom heuristic.
    /// </summary>
    /// <param name="id">ID of this vertex.</param>
    /// <param name="heuristic">The delegate that determines the heuristic.</param>
    public Vertex(T id, Heuristic heuristic)
    {
        this.id = id;
        this.heuristic = heuristic;
    }

    /* Methods */
    /// <summary>
    /// Uses the id to check if this vertex is visited or not.
    /// </summary>
    /// <param name="id">ID.</param>
    /// <returns>True if vertex has been visited by the thing with ID, else false.</returns>
    public bool GetVisited(string id)
    {
        if (!visited.ContainsKey(id)) return false;

        return visited[id];
    }

    /// <summary>
    /// Uses the id to set this vertex's visited status.
    /// </summary>
    /// <param name="id">ID.</param>
    /// <param name="visited">What to set visited to.</param>
    public void SetVisited(string id, bool visited)
    {
        this.visited[id] = visited;
    }

    public override string ToString()
    {
        return $"Vertex: {id}";
    }
    public override int GetHashCode()
    {
        return id.GetHashCode();
    }
}