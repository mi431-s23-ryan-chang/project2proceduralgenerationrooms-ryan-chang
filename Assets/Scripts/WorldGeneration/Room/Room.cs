﻿using System.Collections;
using UnityEngine;
// using UnityEngine.Rendering.Universal;

public class Room : MonoBehaviour
{
    [Header("User settings")]
    [Tooltip("Possible decorations to select from.")]
    public Selector<GameObject> decorations;

    [Tooltip("Optional container for decorations. If specified, it will be deleted once a selection " +
        "is made to save on memory.")]
    public GameObject decorationContainer;

    [Tooltip("Relative probability this room will appear.")]
    public float probability = 0.5f;

    //[Tooltip("Determines the shape of the room. For example, <0, 0, 0> and <1, 0, 0> " +
    //    "denotes a room that occupies its current space and a space to the right of it.")]
    //public Vector3[] shape = { Vector3.zero };

    [Tooltip("Denotes the center of the room, where the shape is (0, 0, 0).")]
    public Vector3 offset;

    [Tooltip("All room spaces.")]
    public RoomSpace[] shape;
    
    [Header("Autogenerated values")]
    [Tooltip("Position on the grid, snapped to the center of the grid space.")]
    public Vector3 gridPosition;

    private static int count = 0;

    private void Start()
    {
        shape = GetComponentsInChildren<RoomSpace>();

        foreach (var decoration in decorations.DoSelection())
        {
            if (decoration)
            {
                decoration.name += count;
                decoration.SetActive(true);

                if (decorationContainer)
                {
                    if (decoration.HasComponentInChildren<Entity>())
                        decoration.transform.SetParent(null, true);
                    else
                        decoration.transform.SetParent(transform, true);
                }
            }
        }

        if (decorationContainer)
            Destroy(decorationContainer);
    }
}