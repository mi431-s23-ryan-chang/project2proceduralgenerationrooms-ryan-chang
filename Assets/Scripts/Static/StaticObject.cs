using UnityEngine;

public class StaticObject : MonoBehaviour
{
    private void Start()
    {
        foreach (var mesh in GetComponentsInChildren<MeshRenderer>())
        {
            if (!mesh.gameObject.HasComponent<Collider>())
            {
                mesh.gameObject.AddComponentIfMissing<MeshCollider>();
            }
            mesh.gameObject.layer = LayerMask.NameToLayer("Static");
        }
    }
}