using UnityEngine;

public class Ladder : Area
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.HasComponent(out ClimbControlModule climb))
        {
            climb.canClimb = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.HasComponent(out ClimbControlModule climb))
        {
            climb.canClimb = false;
        }
    }
}