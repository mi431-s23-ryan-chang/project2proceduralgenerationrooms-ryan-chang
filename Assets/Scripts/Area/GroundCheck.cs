using UnityEngine;

public class GroundCheck : Area
{
    [Tooltip("How many colliders are within this trigger?")]
    public int internalCount;

    [Tooltip("If true, then considered to be grounded.")]
    public bool IsGrounded => internalCount > 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Static"))
        {
            internalCount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Static"))
        {
            internalCount--;
        }
    }
}