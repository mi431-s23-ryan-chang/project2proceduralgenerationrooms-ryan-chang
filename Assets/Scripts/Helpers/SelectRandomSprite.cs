﻿using System.Collections;
using UnityEngine;

public class SelectRandomSprite : MonoBehaviour
{
    public Sprite[] sprites;

    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = sprites.GetRandomValue();
    }
}