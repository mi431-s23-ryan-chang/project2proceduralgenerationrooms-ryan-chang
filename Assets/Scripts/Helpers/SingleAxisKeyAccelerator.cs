﻿using System.Collections.Generic;
using UnityEngine;

public class SingleAxisKeyAccelerator : MonoBehaviour
{
    /// <summary>
    /// The amount of time that has passed since the key has been pressed.
    /// </summary>
    private float timeElasped = 0;

    /// <summary>
    /// After how many seconds will the button be considered to be held down?
    /// </summary>
    private float maxTime = 1;

    /// <summary>
    /// The key value that is being currently read
    /// </summary>
    private float internalKeyVal;

    /// <summary>
    /// The readable value
    /// </summary>
    public float KeyValue
    {
        get
        {
            if (KeyPressed && timeElasped == 0)
            {
                return internalKeyVal;
            }
            else if (KeyPressed && timeElasped > maxTime)
            {
                return internalKeyVal;
            }
            return 0;
        }
    }

    /// <summary>
    /// If true, the key is pressed down
    /// </summary>
    public bool KeyPressed { get; private set; }

    /// <summary>
    /// Initializes the key accelerator
    /// </summary>
    /// <param name="maxTime">After how many seconds will the button be considered to be held down?</param>
    public void Initialize(float maxTime)
    {
        this.maxTime = maxTime;
    }

    /// <summary>
    /// Call this method in the input message function
    /// </summary>
    /// <param name="value"></param>
    public void CallAccelerator(float? value)
    {
        if (value != null)
        {
            internalKeyVal = (float)value;
            KeyPressed = true;
        }
        else
        {
            internalKeyVal = 0;
            KeyPressed = false;
        }
    }

    private void Update()
    {
        if (KeyPressed)
        {
            timeElasped += Time.deltaTime;
        }
        else
        {
            timeElasped = 0;
        }
    }
}