using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager singleton;

    public Entity Player { get; set; }

    private void Awake()
    {
        singleton = this;

        if (GameObject.FindGameObjectWithTag("Player"))
            Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Entity>();
        else
            Debug.LogError("You do not have a gameobject tagged Player in the scene.");
    }
}